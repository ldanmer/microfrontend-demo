import { getNthFobonacciNumber } from "./helpers/get-nth-fibonacci-number";
import { initWorkerFn } from "./promisified-worker";

import { initFibonacciSharedWorker } from "./init-fibonacci-shared-worker";

import "./styles/style.scss";

const workerResult = document.getElementById('worker-result');
// Shared worker
const sharedWorker = initFibonacciSharedWorker(
  (data) => {
    workerResult.innerHTML = JSON.stringify(data, null, 2);
  },
  (errorMessage) => {
    workerResult.innerHTML = errorMessage;
  }
);

const buttonClickProcess = (event) => {
  const container = event.currentTarget.closest('figure');
  const input = container.querySelector('input').value;
  const result = container.querySelector('.result-el');

  result.innerHTML = "processing...";

  sharedWorker.port.postMessage(input);

  runPromisifiedWorkerFn(Number(input))
    .then((value) => {
      result.innerHTML = value;
    })
    .catch((errorMessage) => {
      result.innerHTML = errorMessage;
    });
}
const runPromisifiedWorkerFn = initWorkerFn(getNthFobonacciNumber);

const buttons = document.querySelectorAll(".calc-btn");
Array.from(buttons).forEach((button) => {
  button.addEventListener('click', buttonClickProcess)
});
