# Web workers example

## How to use it
1. navigate to web-workers root folder
2. run `yarn install` (or `npm install`)
3. run `yarn start` (or `npm run start`)
4. Open up http://localhost:3000 in a web browser.
