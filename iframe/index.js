(() => {
  const button = document.getElementById('btn');
  const logger = document.getElementById('logger');

  const output = text => {
    const str = text + logger.innerHTML;
    logger.innerHTML = str.substring(0, 300);
  };

  button.addEventListener('click', () => {
    output('Loading...<br>');
    setTimeout(() => Array(5000000).fill(0).map(() => Math.random()).sort(), 100);
  });

  let prevTime = +(new Date());

  setInterval(() => {
    const now = +(new Date());
    output(`100ms ${prevTime - now}<br>`);
    prevTime = now;
  }, 100);
})();