import {NgModule, enableProdMode} from '@angular/core';
import {APP_BASE_HREF} from "@angular/common";
import {RouterModule, Routes} from "@angular/router";
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";

// @ts-ignore
import {AppComponent} from "./app.component.ts";
const appRoutes: Routes = [];

enableProdMode();

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RouterModule.forRoot(appRoutes, {}),
    ],
    providers: [{provide: APP_BASE_HREF, useValue: '/angular/'}],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export default class MainModule {
}
